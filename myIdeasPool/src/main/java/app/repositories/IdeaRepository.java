package app.repositories;

import org.springframework.data.repository.CrudRepository;

import app.dto.Idea;

public interface IdeaRepository extends CrudRepository<Idea, Long> {

}
