package app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.dto.RefreshToken;
import app.repositories.RefreshTokenRepository;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {

	@Autowired
	private RefreshTokenRepository refreshTokenRepo;
	
	
	@Override
	public RefreshToken getById(String refreshTokenStr) {
		System.out.println("Sourabh  refreshTokem = " + refreshTokenStr);
		return refreshTokenRepo.findById(refreshTokenStr).get();
	}

	@Override
	public RefreshToken save(RefreshToken refreshToken) {
		
		RefreshToken rTok = refreshTokenRepo.save(refreshToken);
		return rTok;
	}

	@Override
	public void delete(String refreshTokenStr) {
		refreshTokenRepo.deleteById(refreshTokenStr);
		
	}

	public RefreshTokenRepository getRefreshTokenRepo() {
		return refreshTokenRepo;
	}

	public void setRefreshTokenRepo(RefreshTokenRepository refreshTokenRepo) {
		this.refreshTokenRepo = refreshTokenRepo;
	}

	@Override
	public RefreshToken getByUserEmail(String email) {
		
		return refreshTokenRepo.findByEmail(email);
	}

}
