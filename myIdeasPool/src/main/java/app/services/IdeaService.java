package app.services;

import java.util.List;

import app.dto.Idea;

public interface IdeaService {

	List<Idea> listAll();

	Idea getById(Long ideaId);

	Idea save(Idea idea);
	Idea update(Idea idea, Long id);

	void delete(Long ideaId);

	List<Idea> getPaginatedIdeas(int page);
}
