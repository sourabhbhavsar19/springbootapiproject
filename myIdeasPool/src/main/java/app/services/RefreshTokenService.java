package app.services;

import app.dto.RefreshToken;

public interface RefreshTokenService {

	RefreshToken getById(String refreshTokenStr);
	
	RefreshToken getByUserEmail(String email);

	RefreshToken save(RefreshToken refreshToken);
	

	void delete(String refreshTokenStr);
}
