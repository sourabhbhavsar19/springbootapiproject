package app.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.dto.Idea;
import app.repositories.IdeaRepository;

@Service
public class IdeaServiceImpl implements IdeaService {

	private IdeaRepository ideaRepository;
	
	@Autowired
	public IdeaServiceImpl(IdeaRepository ideaRepository) {
		this.ideaRepository = ideaRepository;
	}
	
	@Override
	public List<Idea> listAll() {
		List<Idea> ideas = new ArrayList<Idea>();
		ideaRepository.findAll().forEach(ideas::add); 
        
		return ideas;
	}

	@Override
	public Idea getById(Long ideaId) {
		// TODO Auto-generated method stub
		return ideaRepository.findById(ideaId).get();
	}

	@Override
	public Idea save(Idea idea) {
		// TODO Auto-generated method stub
		Idea savedIdea = ideaRepository.save(idea);
		
		return savedIdea;
	}
	
	

	@Override
	public void delete(Long ideaId) {
		// TODO Auto-generated method stub
		ideaRepository.deleteById(ideaId);
		
	}

	@Override
	public Idea update(Idea idea, Long id) {
		// TODO Auto-generated method stub
		Idea ideaToUpdate = getById(id);
		ideaToUpdate.setContent(idea.getContent());
		ideaToUpdate.setEase(idea.getEase());
		ideaToUpdate.setImpact(idea.getImpact());
		ideaToUpdate.setConfidence(idea.getConfidence());
		ideaToUpdate.setAverage_score((idea.getConfidence() + idea.getEase() + idea.getImpact()) / 3);
		
		
		
		
		return ideaRepository.save(ideaToUpdate);
	}

	@Override
	public List<Idea> getPaginatedIdeas(int page) {

		List<Idea> allIdeas =  listAll();
		
		int pageSize = 10;
		int numPages = (allIdeas.size() / pageSize) + 1;
		
		
		if (page > numPages)
		{
			return new ArrayList<Idea>();
		}
		else if (page == numPages)
		{
			return allIdeas.subList(page * pageSize, allIdeas.size());
		}
		else
		{
			return allIdeas.subList(page * pageSize, (page + 1) * pageSize);
		}
		
	}

}
