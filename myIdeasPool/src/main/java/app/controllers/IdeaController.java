package app.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import app.dto.Idea;
import app.dto.UserDetails;
import app.services.IdeaService;
import app.services.UserService;

@RestController
public class IdeaController {
	
	@Autowired
	private IdeaService ideaService;

	
	
	@RequestMapping(method = RequestMethod.GET, path = "/ideas")
	public List<Idea> getAllIdeas()
	{
		return ideaService.listAll();
	}
	
	
	@RequestMapping(method = RequestMethod.GET,  params = {"page"}, path = "/ideas")
	public List<Idea> getAllIdeassPaginated( @RequestParam("page") int page)
	{
		return ideaService.getPaginatedIdeas(page);
			
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/ideas")
	@ResponseStatus(code = HttpStatus.CREATED)
	@Consumes(value="appliction/json")
	@Produces(value="appliction/json")
	public Idea addIdea(@RequestBody Idea idea) {
	
		return ideaService.save(idea);
	}

	
	@RequestMapping(method = RequestMethod.PUT, path = "/ideas/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	@Consumes(value="appliction/json")
	@Produces(value="appliction/json")
	public Idea updateIdea(@RequestBody Idea idea, @PathVariable(name = "id")  Long id) {
	
		return ideaService.update(idea, id);
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/ideas/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteIdea(@PathVariable(name = "id") Long id) {
	
		 ideaService.delete(id);
	}
	

	
	public IdeaService getIdeaService() {
		return ideaService;
	}

	public void setIdeaService(IdeaService ideaService) {
		this.ideaService = ideaService;
	}

}
